var adPage = document.getElementById('adPage');
var expandContainer = document.getElementById('expandContainer');
var preAccessContainer = document.getElementById('preAccess');
var cameraViewContainer = document.getElementById('cameraView');
var postSnapContainer = document.getElementById('postSnap');

 function expandClick() {
	adPage.style.display = 'none';
	expandContainer.style.display = 'block';
}

function continueClick() {
	preAccessContainer.style.display = 'none';
	cameraViewContainer.style.display = 'block';
	startExperience('environment',success,function(){console.log('error')});
}

function shutterClick() {
	cameraViewContainer.style.display = 'none';
	postSnapContainer.style.display = 'block';
}

function success(v,stream){
    var url = window.URL || window.webkitURL;
    v.src = url ? url.createObjectURL(stream) : stream;
    v.autoplay = "autoplay";
    v.play();
}