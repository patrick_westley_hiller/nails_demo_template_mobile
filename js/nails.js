
function invokeCamera(constraints, success, error) {
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || null;
    if (navigator.getUserMedia == null) {
        error();
    }
    else {
        var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
            constraints = {
                video: true,
                audio: false
            }
        }
        var media = navigator.getUserMedia(constraints, function(stream){
            var stream = stream;
            success(document.getElementById('camera'),stream);
        }, error);
    }
}
function startExperience(camera_side, success, error) {
    var constraints = null;
    var chosen_id = null;
    var queryterm_1 = null;
    var queryterm_2 = null;
    if (camera_side == 'user') {
        queryterm_1 = 'front';
        queryterm_2 = 'user';
    } else {
        queryterm_1 = 'rear';
        queryterm_2 = 'environment';
    }

    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
        constraints = {
            video: true,
            audio: false
        }
        goCam('hi');
    }else{
        MediaStreamTrack.getSources(function(sources) {
            for (var i = 0; i !== sources.length; ++i) {
                source = sources[i];
                if (source.kind == 'video' || source.kind.toLocaleLowerCase().includes("camera")) {
                    chosen_id = source.id;

                    if (source.label.toLocaleLowerCase().includes(queryterm_1) || source.label.toLocaleLowerCase().includes(queryterm_2) || source.facing.toLocaleLowerCase().includes(queryterm_1) || source.facing.toLocaleLowerCase() == queryterm_2) {
                        chosen_id = source.id;
                        break
                    }
                }
            }
            var opt = [{
                sourceId: chosen_id
            }];
            constraints = {
                video: {optional: opt},
                audio: false
            };
            goCam(chosen_id);
        });
    }

    function goCam(chosen_id) {
        if (chosen_id !== null) {
            invokeCamera(constraints, success, error)
        } else {
            error();
        }
    }
}

function success(v,stream){
    var url = window.URL || window.webkitURL;
    v.src = url ? url.createObjectURL(stream) : stream;
    v.play();
}